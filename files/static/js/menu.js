(function() {
    'use strict';

    var url = location.pathname.slice(0, -1);
    var links = document.querySelectorAll('.nav.navbar-nav a');

    [].forEach.call(links, function (link) {
        if (link.getAttribute('href') == url) {
            link.classList.add('active');
        }
    });
})();