from django.shortcuts import render

from .models import organizers

def index(request):
	org_list = organizers.objects.all()
	context = {'organizers' : org_list}
	return render(request, 'organizers/organizers.html',context)
