# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='organizers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'\xd0\xa4\xd0\x98\xd0\x9e')),
                ('role', models.CharField(max_length=150, verbose_name=b'\xd0\xa0\xd0\xbe\xd0\xbb\xd1\x8c')),
                ('image', models.ImageField(null=True, upload_to=b'organizers/img', blank=True)),
            ],
            options={
                'verbose_name': '\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0442\u043e\u0440',
                'verbose_name_plural': '\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0442\u043e\u0440\u044b',
            },
        ),
    ]
