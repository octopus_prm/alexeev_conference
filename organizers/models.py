# -*- coding: utf-8 -*-
from django.db import models

class organizers(models.Model):
	name = models.CharField(max_length=50, verbose_name='ФИО')
	role = models.CharField(max_length=150, verbose_name='Роль')
	image = models.ImageField(upload_to="organizers/img", null=True, blank=True)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name='Организатор'
		verbose_name_plural='Организаторы'



