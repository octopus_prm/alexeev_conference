from django.conf.urls import patterns, url
from speakers import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='speakers'),
	url(r'^add-speaker/$', views.add_speaker, name='add_speaker'),
	url(r'^delete/(?P<del_id>\d+)/$', views.del_speaker, name='delete'),
	url(r'^edit/(?P<edit_id>\d+)/$', views.edit_speaker, name='edit'),
)