# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.cache import cache_page

from .forms import SpeakersForm
from .models import Speakers

#@cache_page(60*15)
def index(request):
	instance = Speakers.objects.all()[::-1]
	paginator = Paginator(instance, 5)

	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		items = paginator.page(1)
	except EmptyPage:
		items = paginator.page(paginator.num_pages)
	
	return render(request, 'speakers/base_speakers.html', {
		"instance": instance,
		"items": items,
		})

def add_speaker(request):
	if request.method == 'POST':
		form = SpeakersForm(request.POST, request.FILES)	
		if form.is_valid():
			form.save()
			return redirect('speakers')
	else:
		form = SpeakersForm()
	return render(request,'speakers/add_speaker.html', {
		'form': form,
		})

def del_speaker(request, del_id):
	speaker = Speakers.objects.get(id = del_id).delete()
	return redirect('speakers')

def edit_speaker(request, edit_id):
	speaker = Speakers.objects.get(id = edit_id)
	if request.method == 'POST':
		form = SpeakersForm(request.POST, request.FILES, instance = speaker)
		if form.is_valid():
			speaker = form.save()
			return redirect('speakers')
	else:
		form = SpeakersForm(instance = speaker)
	return render(request, 'speakers/add_speaker.html', {
		"form": form
		})
