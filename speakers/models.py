# -*- coding: utf-8 -*-
from django.db import models

class Speakers(models.Model):
	full_name = models.CharField(max_length=120, verbose_name='ФИО')
	about = models.TextField(verbose_name='Описание докладчика', blank=True, null=True)
	image = models.ImageField(upload_to="speakers/img", blank=True, null=True, verbose_name='Фото')

	def __unicode__(self):
		return self.full_name

	class Meta:
		verbose_name='Докладчик'
		verbose_name_plural='Докладчики'