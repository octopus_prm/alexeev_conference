# -*- coding: utf-8 -*-
from django import forms

from .models import Speakers

class SpeakersForm(forms.ModelForm):
	class Meta:
		model = Speakers
		fields = ['full_name', 'about', 'image']
	
