# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('speakers', '0003_auto_20151114_2047'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='speakers',
            name='lastname',
        ),
        migrations.RemoveField(
            model_name='speakers',
            name='name',
        ),
        migrations.AddField(
            model_name='speakers',
            name='full_name',
            field=models.CharField(default=datetime.datetime(2015, 12, 8, 18, 45, 5, 48000, tzinfo=utc), max_length=120, verbose_name=b'\xd0\xa4\xd0\x98\xd0\x9e'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='speakers',
            name='about',
            field=models.TextField(null=True, verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xb4\xd0\xbe\xd0\xba\xd0\xbb\xd0\xb0\xd0\xb4\xd1\x87\xd0\xb8\xd0\xba\xd0\xb0', blank=True),
        ),
    ]
