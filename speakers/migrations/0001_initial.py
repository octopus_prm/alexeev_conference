# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Speakers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=b'\xd0\x98\xd0\xbc\xd1\x8f')),
                ('lastname', models.CharField(max_length=100, verbose_name=b'\xd0\xa4\xd0\xb0\xd0\xbc\xd0\xb8\xd0\xbb\xd0\xb8\xd1\x8f')),
                ('about', models.TextField(verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xb4\xd0\xbe\xd0\xba\xd0\xbb\xd0\xb0\xd0\xb4\xd1\x87\xd0\xb8\xd0\xba\xd0\xb0')),
            ],
            options={
                'verbose_name': '\u0414\u043e\u043a\u043b\u0430\u0434\u0447\u0438\u043a',
                'verbose_name_plural': '\u0414\u043e\u043a\u043b\u0430\u0434\u0447\u0438\u043a\u0438',
            },
        ),
    ]
