# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('speakers', '0005_auto_20151208_2148'),
    ]

    operations = [
        migrations.AlterField(
            model_name='speakers',
            name='full_name',
            field=models.CharField(max_length=120, verbose_name=b'\xd0\xa4\xd0\x98\xd0\x9e'),
        ),
    ]
