# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('speakers', '0009_auto_20151209_0008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='speakers',
            name='image',
            field=models.ImageField(default=b'speakers/img/no-img.jpg', upload_to=b'speakers/img', null=True, verbose_name=b'\xd0\xa4\xd0\xbe\xd1\x82\xd0\xbe', blank=True),
        ),
    ]
