# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('speakers', '0006_auto_20151208_2211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='speakers',
            name='image',
            field=models.ImageField(upload_to=b'speakers/img'),
        ),
    ]
