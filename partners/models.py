# -*- coding: utf-8 -*-
from django.db import models

class partners(models.Model):
	name = models.CharField(max_length=150, verbose_name='Партнер/Спонсор')
	text = models.TextField()
	image = models.ImageField(upload_to="partners/img", null=True, blank=True)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name='Партнер/Спонсор'
		verbose_name_plural='Партнеры/Спонсоры'