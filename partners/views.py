from django.shortcuts import render

from .models import partners

def index(request):
	part_list = partners.objects.all()
	context = {'partners' : part_list}
	return render(request, 'partners/partners.html', context)
