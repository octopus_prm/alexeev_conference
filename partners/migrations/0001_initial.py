# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='partners',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name=b'\xd0\x9f\xd0\xb0\xd1\x80\xd1\x82\xd0\xbd\xd0\xb5\xd1\x80/\xd0\xa1\xd0\xbf\xd0\xbe\xd0\xbd\xd1\x81\xd0\xbe\xd1\x80')),
                ('text', models.TextField()),
                ('image', models.ImageField(null=True, upload_to=b'partners/img', blank=True)),
            ],
            options={
                'verbose_name': '\u041f\u0430\u0440\u0442\u043d\u0435\u0440/\u0421\u043f\u043e\u043d\u0441\u043e\u0440',
                'verbose_name_plural': '\u041f\u0430\u0440\u0442\u043d\u0435\u0440\u044b/\u0421\u043f\u043e\u043d\u0441\u043e\u0440\u044b',
            },
        ),
    ]
