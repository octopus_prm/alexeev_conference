from django.conf.urls import patterns, url
from partners import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='partners'),
)