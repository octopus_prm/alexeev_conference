# -*- coding: utf-8 -*-
from django.db import models
import datetime
from django.utils import timezone

class News(models.Model):
	title = models.CharField(max_length=70)
	description = models.CharField(max_length=255,blank=True)
	pub_date = models.DateTimeField()
	text = models.TextField()
	image = models.ImageField(upload_to="news/img", null = True, blank=True)
	is_public = models.BooleanField(default=False)

	def __unicode__(self):
		return self.title

	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
	
	class Meta:
		verbose_name_plural = "Новости"
		verbose_name="Новость"