from django.shortcuts import render

from .models import News

def index(request):
	news_list = News.objects.all()[::-1]
	context = {'news' : news_list}
	return render(request,'news/base_news.html',context)

def publish(request, news_id):
	n = News.objects.get(id = news_id)
	n.is_public = True
	n.save()