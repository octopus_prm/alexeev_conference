# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0003_news_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='is_public',
            field=models.BooleanField(default=False),
        ),
    ]
