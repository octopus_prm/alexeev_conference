from django.contrib import admin


from .models import News

class NewsAdmin(admin.ModelAdmin):
	list_display = ('title','pub_date','was_published_recently','is_public')
	list_filter = ['pub_date']
	search_fields = ['text']

admin.site.register(News,NewsAdmin)


