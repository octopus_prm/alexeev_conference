# -*- coding: utf-8 -*-
from django.db import models

class perform(models.Model):
	name = models.CharField(max_length=150, verbose_name='ФИО')
	theme = models.CharField(max_length=150, verbose_name='Тема')
	time = models.DateTimeField()

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name='Выступление'
		verbose_name_plural='Выступления'