from django.shortcuts import render

from .models import perform

def index(request):
	pefr_list = perform.objects.all()
	context = {'perform' : pefr_list}
	return render(request, 'performances/performances.html', context)