# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='perform',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name=b'\xd0\xa4\xd0\x98\xd0\x9e')),
                ('theme', models.CharField(max_length=150, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xbc\xd0\xb0')),
                ('time', models.TimeField()),
            ],
            options={
                'verbose_name': '\u0412\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0412\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f',
            },
        ),
    ]
