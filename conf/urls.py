from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.views.generic import TemplateView
import django.views.defaults
from django.contrib.flatpages import views
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='index.html'), name="default"),
    url(r'^speakers/',include('speakers.urls')),
	url(r'^contacts/',include('contacts.urls')),
	url(r'^news/', include('news.urls')),
	url(r'^admin/', include(admin.site.urls)),
    url(r'^organizers/', include('organizers.urls')),
    url(r'^partners/', include('partners.urls')),
    url(r'^performances/', include('performances.urls')),
    url(r'^404/$', django.views.defaults.page_not_found),
    url(r'^500/$', django.views.defaults.server_error),
    url(r'^about-us/$', views.flatpage, {'url': '/about/'}, name='about'),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()


