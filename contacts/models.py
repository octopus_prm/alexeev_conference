# -*- coding: utf-8 -*-
from django.db import models

class contact(models.Model):
	name = models.CharField(max_length=100,verbose_name='Имя')
	lastname = models.CharField(max_length=100,verbose_name='Фамилия')
	adress = models.CharField(max_length=100,verbose_name='Адрес',blank=True)
	num = models.BigIntegerField(verbose_name='Номер телефона')
	image = models.ImageField(upload_to="contacts/img", null = True, blank=True)

	@property
	def full_name(self):
		return '%s %s' % (self.name,self.lastname)

	def __unicode__(self):
		return self.full_name

	class Meta:
		verbose_name='Контакт'
		verbose_name_plural='Контакты'
	
