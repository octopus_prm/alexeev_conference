from django.shortcuts import render

from .models import contact

def index(request):
	contact_list = contact.objects.all()
	content = {'contacts' : contact_list}
	return render(request,'contacts/base_contacts.html',content)
